md bin
javac -classpath LeapSDK\lib\LeapJava.jar -d bin src\Application.java src\LeapByteBuffer.java src\LeapListener.java src\UdpServer.java
copy LeapSDK\lib\LeapJava.jar bin\
copy LeapSDK\lib\x64\Leap.dll bin\
copy LeapSDK\lib\x64\LeapJava.dll bin\
pause
