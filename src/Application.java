import java.io.IOException;
import java.util.Scanner;

import com.leapmotion.leap.*;

public class Application {
	UdpServer udpServer;
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		System.out.print("Enter IP address of phone: ");
		String host = reader.next();
		System.out.print("Enter port on which phone listens: ");
		int sendPort = reader.nextInt();
		
        LeapListener listener = new LeapListener(host, sendPort);
		Controller controller = new Controller(listener);
		
		if(!controller.isConnected()) {
			System.out.println("Waiting for controller to connect...");
		}
		
        System.out.println("Press Enter to quit.");

		
		/*
		System.out.println(Bone.Type.TYPE_METACARPAL.ordinal());
		System.out.println(Bone.Type.TYPE_PROXIMAL.ordinal());
		System.out.println(Bone.Type.TYPE_INTERMEDIATE.ordinal());
		System.out.println(Bone.Type.TYPE_DISTAL.ordinal());
		*/
		
		/*
		System.out.println(Pointable.Zone.ZONE_NONE.ordinal());
		System.out.println(Pointable.Zone.ZONE_HOVERING.ordinal());
		System.out.println(Pointable.Zone.ZONE_TOUCHING.ordinal());
		*/
		
		/*
		System.out.println(Finger.Type.TYPE_THUMB.ordinal());
		System.out.println(Finger.Type.TYPE_INDEX.ordinal());
		System.out.println(Finger.Type.TYPE_MIDDLE.ordinal());
		System.out.println(Finger.Type.TYPE_RING.ordinal());
		System.out.println(Finger.Type.TYPE_PINKY.ordinal());
		*/
        try {
            System.in.read();
    		reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        if(listener.server != null) {
        	listener.server.disconnect();
        }
        
        controller.removeListener(listener);
	}
}
