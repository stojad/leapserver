import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.leapmotion.leap.Matrix;
import com.leapmotion.leap.Vector;

public class LeapByteBuffer  {
	ByteBuffer buffer;
	
	public LeapByteBuffer(int numBytes) {
		buffer = ByteBuffer.allocate(numBytes);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
	}
	
	public void putFloat(float value) {
		buffer.putFloat(value);
	}
	
	public void putLong(long value) {
		buffer.putLong(value);
	}
	
	public void putInt(int value) {
		buffer.putInt(value);
	}
	
	public byte[] array() {
		return buffer.array();
	}
	
	public int position() {
		return buffer.position();
	}
	
	// Put a matrix in a byte buffer.
	public void putMatrix(Matrix matrix) {
    	Vector xBasis = matrix.getXBasis();
		Vector yBasis = matrix.getYBasis();
		Vector zBasis = matrix.getZBasis();
		
    	buffer.putFloat(xBasis.getX());
		buffer.putFloat(xBasis.getY());
		buffer.putFloat(xBasis.getZ());
		buffer.putFloat(yBasis.getX());
		buffer.putFloat(yBasis.getY());
		buffer.putFloat(yBasis.getZ());
		buffer.putFloat(zBasis.getX());
		buffer.putFloat(zBasis.getY());
		buffer.putFloat(zBasis.getZ());
    }
    
    // Put a vector in a byte buffer.
    public  void putVector(Vector vector) {
    	buffer.putFloat(vector.getX());
    	buffer.putFloat(vector.getY());
    	buffer.putFloat(vector.getZ());
    }
    
    // Put a boolean value in the buffer by representing it as a 0 for false and a 1 for true.
    public void putBoolean(boolean bool) {
    	buffer.put((byte) (bool ? 1 : 0));
    }
}
