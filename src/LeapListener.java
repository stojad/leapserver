import com.leapmotion.leap.*;

class LeapListener extends Listener {
	private String host;
	private int sendPort;
	private int listenPort = 4200;
	
	public UdpServer server;
	private boolean isServerConnected = false;
	private static final boolean DEBUG_PRINT = false;

	public LeapListener(String host, int sendPort) {
		this.host = host;
		this.sendPort = sendPort;
	}
	
    public void onConnect(Controller controller) {
        System.out.println("Controller connected. Starting UDP server...");
        controller.setPolicy(Controller.PolicyFlag.POLICY_BACKGROUND_FRAMES);
        
        try {
			server = new UdpServer(host, sendPort, listenPort);
			isServerConnected = true;
			System.out.println("Server broadcasting on port " + sendPort);
		} catch (Exception e) {
			System.out.println("Couldn't connect to socket on port " + sendPort + ": " + e.getMessage());
		}
    }

    
    public void onFrame(Controller controller) {
    	long startTime = System.currentTimeMillis();
    	
        if(!isServerConnected) {
        	return;
        }
        
        Frame frame = controller.frame();
        
        LeapByteBuffer buffer = new LeapByteBuffer(6000);
        
       
        // Process data specific to one frame.
        buffer.putFloat(frame.currentFramesPerSecond());
        buffer.putLong(frame.id());
        //buffer.putBoolean(frame.isValid());
        buffer.putLong(frame.timestamp());
        
        buffer.putInt(frame.hands().count()); // Not actual data but the deserializer needs to know how many hands are present.
        buffer.putInt(frame.pointables().count()); // Not actual data but the deserializer needs to know how many hands are present.


        
        // Serialize this frame's hands.
        for(Hand hand : frame.hands()) {
        	// Serialize this hand's arm.
        	Arm arm = hand.arm();
			buffer.putMatrix(arm.basis());
			buffer.putVector(arm.center());
			buffer.putVector(arm.direction());
			buffer.putVector(arm.elbowPosition());
			buffer.putFloat(arm.width());
			buffer.putVector(arm.wristPosition());
		
			// putMatrix(hand.basis());
			buffer.putFloat(hand.confidence());
			buffer.putVector(hand.direction());
			//buffer.putLong(hand.frame().id());
			buffer.putFloat(hand.grabStrength());
			buffer.putInt(hand.id());
			buffer.putBoolean(hand.isLeft());
			//buffer.putBoolean(hand.isRight());
			//buffer.putBoolean(hand.isValid());
			buffer.putVector(hand.palmNormal());
			buffer.putVector(hand.palmPosition());
			buffer.putVector(hand.palmVelocity());
			buffer.putFloat(hand.palmWidth());
			buffer.putFloat(hand.pinchStrength());
			buffer.putVector(hand.sphereCenter());
			buffer.putFloat(hand.sphereRadius());
			buffer.putVector(hand.stabilizedPalmPosition());
			buffer.putFloat(hand.timeVisible());
			buffer.putVector(hand.wristPosition());
        	 
        }
        
        // Serialize the pointable objects.
        for (Pointable pointable : frame.pointables()) {
        	buffer.putVector(pointable.direction());
        	//buffer.putInt(pointable.frame().id());
        	buffer.putInt(pointable.hand().id());
        	buffer.putInt(pointable.id());
        	buffer.putBoolean(pointable.isExtended());
        	buffer.putBoolean(pointable.isFinger());
        	//buffer.putBoolean(pointable.isTool());
        	//buffer.putBoolean(pointable.isValid());
        	buffer.putFloat(pointable.length());
        	buffer.putVector(pointable.stabilizedTipPosition());
        	buffer.putFloat(pointable.timeVisible());
        	buffer.putVector(pointable.tipPosition());
        	buffer.putVector(pointable.tipVelocity());
        	buffer.putFloat(pointable.touchDistance());
        	buffer.putInt(pointable.touchZone().ordinal());
        	buffer.putFloat(pointable.width());
        	
        	// Add extension properties for pointables that are fingers.
        	if(pointable.isFinger()) {
        		Finger finger = frame.finger(pointable.id());
        		
        		buffer.putInt(finger.type().ordinal());
        		
        		// Get all the bones for this finger.
        		for (Bone.Type boneIdx : Bone.Type.values()) {
        			Bone bone = finger.bone(boneIdx);
        			
        			buffer.putMatrix(bone.basis());
        			buffer.putVector(bone.center());
        			buffer.putVector(bone.direction());
        			//buffer.putBoolean(bone.isValid());
        			buffer.putFloat(bone.length());
        			buffer.putVector(bone.nextJoint());
        			buffer.putVector(bone.prevJoint());
        			buffer.putInt(bone.type().ordinal());
        			buffer.putFloat(bone.width());
        		}
        	}
        }
        
        server.send(buffer.array());

        if(DEBUG_PRINT) {
        	System.out.println("Serialisation time: " + (System.currentTimeMillis() - startTime) + " ms. Serialised " + buffer.position() + " bytes.");
        }
    }
}