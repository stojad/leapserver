import java.net.*;

public class UdpServer {
	private InetAddress group;
	private DatagramSocket socket;
	
	private int sendPort, listenPort;
	private String host;
	
	public UdpServer(String host, int sendPort, int listenPort) throws Exception {
			socket = new DatagramSocket(listenPort);
			this.sendPort = sendPort;
			this.listenPort = listenPort;
			
			// Set server to broadcast.
			group = InetAddress.getByName(host);
	}
	
	public boolean send(byte[] buffer) {
		 DatagramPacket packet;
         packet = new DatagramPacket(buffer, buffer.length, group, sendPort);
         
         try {
        	 socket.send(packet);
        	 return true;
         } catch(Exception e) {
        	 	System.out.println("Send failed: " + e.getMessage());
        		 return false;
         }
	}
	
	public void disconnect() {
		socket.close();
	}
}
